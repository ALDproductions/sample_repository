#ifndef HEAD_BADMATH
#define HEAD_BADMATH

class Badmath {
public:
  Badmath();
  ~Badmath();
  int add(int, int);
  int mult(int, int);
  int sub(int, int);
};

#endif
