#include "test_stuff.h"

#include "badmath.h"

using namespace CppUnit;
using namespace std;

//------------------ Well that's a lot of setup. Perhaps I'll put it all in a header file --------------

class TestBadMath : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(TestBadMath);
    CPPUNIT_TEST(testAddition);
    CPPUNIT_TEST(testMultiplication);
    CPPUNIT_TEST(testSubtraction);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);

protected:
    void testAddition(void);
    void testMultiplication(void);
    void testSubtraction(void);

private:
    Badmath *badMathObj;
};

void TestBadMath::setUp(void) {
  badMathObj = new Badmath();
}
void TestBadMath::tearDown(void) {
  delete badMathObj;
}
void TestBadMath::testAddition(void) {
  CPPUNIT_ASSERT(6== badMathObj->add(3,3));
  CPPUNIT_ASSERT(1== badMathObj->add(1,0));
  CPPUNIT_ASSERT(4== badMathObj->add(2,2));
}
void TestBadMath::testSubtraction(void) {
  CPPUNIT_ASSERT(6== badMathObj->sub(8,2));
  CPPUNIT_ASSERT(0== badMathObj->sub(1,1));
  CPPUNIT_ASSERT(-1== badMathObj->sub(6,7));
}
void TestBadMath::testMultiplication(void) {
  CPPUNIT_ASSERT(6== badMathObj->mult(2,3));
  CPPUNIT_ASSERT(1== badMathObj->mult(1,1));
  CPPUNIT_ASSERT(4== badMathObj->mult(2,2));
}


CPPUNIT_TEST_SUITE_REGISTRATION( TestBadMath );


/* */

    /*
      int add(int, int);
  int mult(int, int);
  int sub(int, int);
    */
