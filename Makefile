cmake: CMakeLists.txt build
	cd build; cmake ..; make;
manual: src/ manual_build/lib manual_build/bin #Tell Make to check if the directories it needs exist, and to create them if they don't
	#Generate the libraries
	 g++ src/badmath/badmath.cpp -Iinclude -c -fPIC -o manual_build/lib/badmath.o
	 #Generate the executables
	 g++ src/executable.cpp manual_build/lib/badmath.o -Iinclude -o manual_build/bin/badmathtest.exe

manual_build/lib:
	mkdir -p manual_build/lib #Make a new directory

manual_build/bin:
	mkdir -p manual_build/bin #Make a new directory
build:
	mkdir build
make_test: cmake
	./build/bin/testbadmath
