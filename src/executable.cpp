#include "badmath.h"
#include <iostream>
using namespace std;
int main() {
  Badmath *bm = new Badmath();
  cout << "add 2 and 2 - " << bm->add(2,2) << endl <<
          "add 2 and 2 - " << bm->add(3,3) << endl <<
          "mult 3 and 3 - " << bm->mult(3,3) << endl <<
          "mult 2 and 2 - " << bm->mult(2,2) << endl <<
          "sub 1 and 1 - " << bm->sub(1,1) << endl <<
          "sub 2 and 1 - " << bm->sub(2,1) << endl;
  delete bm;
}
